#!/usr/bin/env sh

name="breitbandmessung"
log_path="/opt/breitbandmessung/log"


logfile_default="system"


logging()
{
  if [ $1 = '-v' ]; then
    echo "$3" >> $log_path/$2
  fi
}

if [ ! -e $log_path ]; then
  mkdir -p $log_path
  touch $log_path/speedtest
fi

logging $1 $logfile_default "Start: speedtest-cli"
speedtest-cli >> $log_path/speedtest
