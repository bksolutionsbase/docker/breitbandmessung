FROM ubuntu:latest as SRC

# Install cURL to download *.deb
RUN apt-get update \
    && apt-get install -y curl \
    && rm -rf /var/lib/apt/lists/*

# Download required *.deb, hint use option "--silent" or "--progress-bar"
RUN curl --progress-bar https://download.breitbandmessung.de/bbm/Breitbandmessung-linux.deb -o /opt/breitbandmessung.deb




FROM ubuntu:latest 

# Set labels
LABEL name="briezh/breitbandmessung" \
      vendor="BKSolutions" \
      summary="Breitbandmessung.de" \
      description="Messkampagne der tatsächlich erreichten Geschwindigkeiten Ihres Internetzugangs im Up- und Download." \
      release=1 \
      breitbandmessung.run="docker run -rm --name breitbandmessung -v <$PWD>/results:/results -dt registry.gitlab.com/bksolutionsbase/docker/breitbandmessung:latest" \
      breitbandmessung.docker.cmd="docker run -d docker.io/briezh/breitbandmessung:latest" \
      breitbandmessung.podman.cmd="podman run -d docker.io/briezh/breitbandmessung:latest" \
      breitbandmessung.version="3.2.1" \
      MAINTAINER="Briezh Khenloo"


# Prepare image and environment
RUN mkdir -p /opt/breitbandmessung/log

WORKDIR /opt/breitbandmessung

VOLUME ["/opt/breitbandmessung"]

COPY --chmod=0750 start.sh /opt/breitbandmessung/start.sh


# Set locale and install 
#   - speedtest     internet speed test alternative
#   - net-tools     requirement for bandbreitenmessung
RUN apt-get update && apt-get install -y locales speedtest-cli net-tools \
 && rm -rf /var/lib/apt/lists/* \
 && localedef -i de_DE -c -f UTF-8 -A /usr/share/locale/locale.alias de_DE.UTF-8
ENV LANG de_DE.utf8


# Install Breitbandmessung
COPY --from=SRC /opt/breitbandmessung.deb /tmp/breitbandmessung.deb
RUN dpkg -i /tmp/breitbandmessung.deb \
 && rm -f /tmp/breitbandmessung.deb
 

ENTRYPOINT ["/opt/breitbandmessung/start.sh"]
CMD ["-v"]
