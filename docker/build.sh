#!/usr/bin/env sh

##
# Docker Version
##

set -euxo pipefail

# Default branch leaves tag empty (= latest tag)
# All other branches are tagged with the escaped branch name (commit ref slug)
if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
  tag=""
  echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
else
  tag=":$CI_COMMIT_REF_SLUG"
  echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
fi

# Build image
DOCKER_BUILDKIT=1 docker build -f ./Dockerfile -t "$CI_REGISTRY_IMAGE${tag}" .

# Push gitlab project regsitry
echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
docker push "$CI_REGISTRY_IMAGE${tag}"
docker logout $CI_REGISTRY

# Push to docker.io
echo "$CI_REGISTRY_PASSWORD_DOCKERHUB" | docker login -u "$CI_REGISTRY_USER_DOCKERHUB" --password-stdin $CI_REGISTRY_DOCKERHUB
docker tag "$CI_REGISTRY_IMAGE${tag}" "$CI_REGISTRY_DOCKERHUB/${CI_REGISTRY_PATH_DOCKERHUB}/${CI_PROJECT_NAME}${tag}"
docker push "$CI_REGISTRY_DOCKERHUB/${CI_REGISTRY_PATH_DOCKERHUB}/${CI_PROJECT_NAME}${tag}"
docker logout $CI_REGISTRY_DOCKERHUB
