#!/usr/bin/env bash

##
# Buildah Version
##

set -euxo pipefail

# Default branch leaves tag empty (= latest tag)
# All other branches are tagged with the escaped branch name (commit ref slug)
if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
  tag=""
  echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
else
  tag=":$CI_COMMIT_REF_SLUG"
  echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
fi

# Build image
buildah bud -f ./Dockerfile -t "$CI_REGISTRY_IMAGE${tag}" .

# Push gitlab project regsitry
echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
buildah push "$CI_REGISTRY_IMAGE${tag}"
buildah logout $CI_REGISTRY

# Push to docker.io
echo "$CI_REGISTRY_PASSWORD_DOCKERHUB" | buildah login -u "$CI_REGISTRY_USER_DOCKERHUB" --password-stdin $CI_REGISTRY_DOCKERHUB
buildah push "$CI_REGISTRY_IMAGE${tag}" "$CI_REGISTRY_DOCKERHUB/${CI_REGISTRY_PATH_DOCKERHUB}/${CI_PROJECT_NAME}${tag}"
buildah logout $CI_REGISTRY_DOCKERHUB
